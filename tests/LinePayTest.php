<?php

namespace Pharaoh\Paytool\Tests;

use Illuminate\Support\Arr;
use Pharaoh\Paytool\Drivers\LinePayDriver;

class LinePayTest extends BaseTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testCreateOrder()
    {
        // Arrange
        $params = [
            'merchant_trade_no' => date('YmdHis'),
            'total_amount' => 250,
            'choose_payment' => 'LinePay',
            'name' => '黑芝麻豆漿',
            'trade_desc' => '一瓶',
            'price' => 250,
            'currency' => 'TWD',
            'quantity' => 1
        ];

        // Act
        $driver = \App::make(LinePayDriver::class);
        $responseData = $driver->createOrder($params);
        $url = Arr::get($responseData, 'paymentUrl');

        // Assert
        $this->assertTrue(filter_var($url, FILTER_VALIDATE_URL) !== false);
    }

    public function testHandleResponseData()
    {
        // Arrange
        $redirectUrl = '/line_pay/success';
        $amount = '250';

        $params = [
            'redirect_url' => $redirectUrl,
            'amount' => $amount,
            'currency' => 'TWD',
            'transactionId' => '2024020502061235310',
            'orderId' => '20240205103701'
        ];

        // Act
        $driver = \App::make(LinePayDriver::class);
        $responseData = $driver->handleResponseData($params);

        // Assert
        $this->assertEquals($responseData['returnCode'] === '0000');
    }
}