<?php

namespace Pharaoh\Paytool\Drivers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Pharaoh\Paytool\Exceptions\PaytoolException;

class LinePayDriver extends AbstractDriver
{
    public function __construct()
    {
        $this->vendorCode = 'line_pay';
        $this->settings = config("paytool.driver.{$this->vendorCode}");
    }

    public function createOrder(array $params)
    {
        try {
            $data = [
                'amount' => $amount =  Arr::get($params, 'total_amount'),
                'currency' => $currency = Arr::get($params, 'currency'),
                'orderId' => $orderId = Arr::get($params, 'merchant_trade_no'),
                'packages' => [
                    [
                        'id' => $orderId,
                        'amount' => $amount,
                        'name' => $name = Arr::get($params, 'name'),
                        'products' => [
                            [
                                'name' => $name,
                                'quantity' => Arr::get($params, 'quantity'),
                                'price' => Arr::get($params, 'price'),
                            ],
                        ],
                    ],
                ],
                'redirectUrls' => [
                    'confirmUrl' => config('app.url') . '/paytool/pay-notice/' . $this->vendorCode
                        . '?redirect_url=' . Arr::get($this->settings, 'redirect_uri', '')
                        . '&amount=' . $amount
                        . '&currency=' . $currency,
                    'cancelUrl' => config('app.url') . Arr::get($this->settings, 'cancel_uri', ''),
                    'confirmUrlType' => 'SERVER'
                ]
            ];

            $url = Arr::get($this->settings, 'service_url');
            $uri = '/v3/payments/request';
            $channelId = Arr::get($this->settings, 'channel_id');
            $nonce = Str::uuid()->toString();

            $header = [
                'Content-Type' => 'application/json',
                'X-LINE-ChannelId' => $channelId,
                'X-LINE-Authorization-Nonce' => $nonce,
                'X-LINE-Authorization' => $this->encryptData($uri, $data, $nonce),
            ];

            $response = Http::withHeaders($header)->post($url.$uri, $data)->json();

            $status = Arr::get($response, 'returnCode');
            if ($status !== '0000') {
                throw new \Exception(Arr::get($response, 'returnMessage'));
            }

            return [
                'paymentUrl' => Arr::get($response, 'info.paymentUrl.web')
            ];
        } catch (\Exception $exception) {
            throw new PaytoolException($exception->getMessage());
        }
    }

    public function handleResponseData(array $params)
    {
        try {
            $transactionId = Arr::get($params, 'transactionId');

            $data = [
                'amount' => Arr::get($params, 'amount'),
                'currency' => Arr::get($params, 'currency')
            ];

            $url = Arr::get($this->settings, 'service_url');
            $uri = "/v3/payments/{$transactionId}/confirm";
            $channelId = Arr::get($this->settings, 'channel_id');
            $nonce = Str::uuid()->toString();

            $header = [
                'Content-Type' => 'application/json',
                'X-LINE-ChannelId' => $channelId,
                'X-LINE-Authorization-Nonce' => $nonce,
                'X-LINE-Authorization' => $this->encryptData($uri, $data, $nonce),
            ];

            $response = Http::withHeaders($header)->post($url.$uri, $data)->json();

            $status = Arr::get($response, 'returnCode');
            if ($status !== '0000') {
                throw new \Exception(Arr::get($response, 'returnMessage'));
            }

            $response['redirect_url'] = Arr::get($params, 'redirect_url', '');

            return $response;
        } catch (\Exception $exception) {
            throw new PaytoolException($exception->getMessage());
        }
    }

    /**
     * 加密
     *
     * @param $uri
     * @param $requestBody
     * @param $nonce
     * @return string
     */
    private function encryptData($uri, $requestBody, $nonce): string
    {
        $channelSecret = Arr::get($this->settings, 'channel_secret');
        $authMacText = $channelSecret . $uri . json_encode($requestBody) . $nonce;
        return base64_encode(hash_hmac('sha256', $authMacText, $channelSecret, true));
    }
}